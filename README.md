# Настройка линтера CSS по нашему стайлгайду #

Для использования stylelint с styled-components (SC) необходимо установить следующие пакеты:

* stylelint (сам линтер)
* stylelint-config-recommended (базовый конфиг)
* stylelint-config-styled-components (конфиг для отключения правил, конфликтующих с SC)
* stylelint-order (для проверки порядка правил)
* stylelint-processor-styled-components (извлекает стили из SC)

Дополнительные настройки правил сделаны в файле `.stylelintrc` в корне проекта.

Для отключения проверки линтером добавляем комментарий `/* stylelint-disable */`
```
import React from 'react'
import styled from 'styled-components'

// Disable stylelint from within the tagged template literal
const Wrapper = styled.div`
  /* stylelint-disable */
  background-color: #fff;
`

// Or from the JavaScript around the tagged template literal
/* stylelint-disable */
const Wrapper = styled.div`
  background-color: #fff;
`
```

## Плагины для IDE ##
- [SublimeLinter-stylelint](https://github.com/SublimeLinter/SublimeLinter-stylelint) - Sublime Text plugin for stylelint.
- [SublimeLinter-contrib-stylelint_d](https://github.com/jo-sm/SublimeLinter-contrib-stylelint_d) - Sublime Text plugin for stylelint that run's on daemon.
- [WebStorm](https://blog.jetbrains.com/webstorm/2016/09/webstorm-2016-3-eap-163-4830-stylelint-usages-for-default-exports-and-more/) - version 2016.3 onwards has built-in support for stylelint.
- [vscode-stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint) - VS Code extension for stylelint.
